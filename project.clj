(defproject hugsql-next-jdbc "0.1.3"
  :description "next.jdbc adapter for HugSQL"
  :url "https://gitlab.com/nikperic/hugsql-next-jdbc"
  :license {:name "MIT License"
            :url  "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [seancorfield/next.jdbc "1.0.7"]
                 [com.layerware/hugsql-adapter "0.4.9"]]
  :profiles {:dev {:dependencies [[com.layerware/hugsql-core "0.4.9"]
                                  [com.h2database/h2 "1.4.199"]]}})
