# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## 0.1.3 - 2019-09-13
### Added
- One-arity `hugsql-adapter-next-jdbc` to pass in default command options (e.g. :builder-fn, etc.)

### Fixed
- Default value for command-options as {}, not nil [#2](https://gitlab.com/nikperic/hugsql-next-jdbc/issues/2)

## 0.1.2 - 2019-05-22
### Fixed
- Allow command-options to be passed through for :insert/:i! queries

## 0.1.1 - 2019-05-22
### Fixed
- Fix :insert/:i! command and add test to check for it [#1](https://gitlab.com/nikperic/hugsql-next-jdbc/issues/1)

## 0.1.0 - 2019-05-22
### Added
- Initial release
