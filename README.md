# HugSQL adapter for next.jdbc

An adapter for [HugSQL](https://github.com/layerware/hugsql) with [next.jdbc](https://github.com/seancorfield/next-jdbc) 

## Usage

You can read the [HugSQL documentation](https://www.hugsql.org/#adapter-other) for third-party adapters.

Include in your project's dependencies, replacing version with latest version

[![Clojars Project](https://img.shields.io/clojars/v/hugsql-next-jdbc.svg)](https://clojars.org/hugsql-next-jdbc)

```
[hugsql-next-jdbc "version"]
```

Then simply require it in your project and initialize. Below is an example, you can find other ways to do this in the [HugSQL documentation](https://www.hugsql.org/#adapter-other). 

```clojure
(ns my-app
  (:require [hugsql.core :as hugsql]
            [hugsql.adapter.next-jdbc :as next-adapter]))

(defn app-init []
  (hugsql/set-adapter! (next-adapter/hugsql-adapter-next-jdbc)))
```

You can pass in default command options to the adapter via the one-arity version. For example:

```clojure
(hugsql/set-adapter! (next-adapter/hugsql-adapter-next-jdbc {:builder-fn result-set/as-unqualified-maps}))
```

## License

Copyright © 2019 Nikola Peric